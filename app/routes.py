from app import app

@app.route('/')
def index():
    return """
                <html>
                    <body>
                        <h1>Hi there!</h1>
                        
                        <p>This application's purpose is to help verify that DAST can run successfully against
                           the vendor template DAST.gitlab-ci.yml. This gives us confidence that DAST and GitLab
                           function well together.</p>
                           
                       <p>I hope it serves you well!</p>
                    </body>
                </html>
           """
